#!/usr/bin/python3

import subprocess
import fileinput


for_starters = "      #- ./LocalSettings.php:/var/www/html/LocalSettings.php:ro"
for_running  = "      - ./LocalSettings.php:/var/www/html/LocalSettings.php:ro"

def find_home_folder():
    pwd = str(subprocess.check_output(["pwd"]))
    home_list = pwd.split("/")
    home_folder = "/" + home_list[1] + "/" + home_list[2]
    return home_folder
# mv ~/Downloads/LocalSettings.php LocalSettings.php
home_folder = find_home_folder()
subprocess.run(["mv", home_folder + "/Downloads/LocalSettings.php", "LocalSettings.php"])
subprocess.run(["docker-compose", "down"])
with fileinput.FileInput("docker-compose.yaml", inplace=True, backup='.bak') as file:
    for line in file:
        print(line.replace(for_starters, for_running), end='')
# echo "enableSemantics();" >> LocalSettings.php
# subprocess.run(["echo", "enableSemantics();", ">>", "LocalSettings.php"])

with open("LocalSettings.php", "a") as myfile:
    myfile.write("enableSemantics();")

subprocess.run(["docker-compose", "up", "-d"])

# docker-compose exec smw php maintenance/update.php
