#!/usr/bin/python3

import subprocess
import fileinput
import tarfile
import os
import time
from datetime import datetime

def string_matcher(match_these, with_this):
    # First argument is a list of the small string you are looking for in a big string
    # Second argument is a list of big strings
    return [s for s in with_this if any(xs in s for xs in match_these)]

def get_volume_names():
    raw_docker_volumes = str(subprocess.check_output(["docker", "volume", "ls"])).split()
    docker_volumes = []
    for i in raw_docker_volumes:
        tmp_docker_volumes = i.split("\\n")
        for i in tmp_docker_volumes:
            docker_volumes.append(i)
    volume_names = string_matcher(["semantic-mediawiki", "db", "images"], docker_volumes)
    mah_volumes = {}
    if "db" in volume_names[0]:
        mah_volumes["db_volume_name"] = volume_names[0]
        mah_volumes["image_volume_name"] = volume_names[1]
    else:
        mah_volumes["db_volume_name"] = volume_names[1]
        mah_volumes["image_volume_name"] = volume_names[0]
    return mah_volumes

def docker_backup(volume_name, backup_dir, tar_file):
    #stupid_shash = "\\"
    restore_list = ["docker", "run", "--rm"]
    #restore_list.append(stupid_shash)
    restore_list.append("-v")
    restore_list.append(volume_name + ":/data")
    #restore_list.append(stupid_shash)
    restore_list.append("-v")
    restore_list.append(backup_dir + ":/backup")
    restore_list.append("ubuntu")
    restore_list.append("bash")
    #restore_list.append(stupid_shash)
    restore_list.append("-c")
    #restore_list.append('"cd /data && tar cvf /backup/' + tar_file + ' ."')
    restore_list.append('"cd')
    restore_list.append('/data')
    restore_list.append("&&")
    restore_list.append("tar")
    restore_list.append("cvf")
    restore_list.append("/backup/" + tar_file)
    restore_list.append('."')
    return restore_list

def find_home_folder():
    pwd = str(subprocess.check_output(["pwd"]))
    home_list = pwd.split("/")
    home_folder = "/" + home_list[1] + "/" + home_list[2]
    return home_folder


for_starters = "      #- ./LocalSettings.php:/var/www/html/LocalSettings.php:ro"
for_running  = "      - ./LocalSettings.php:/var/www/html/LocalSettings.php:ro"

bash_code = '''
mkdir ~/semantic-mediawiki-backup
cp LocalSettings.php ~/semantic-mediawiki-backup/
echo "Stopping Semantic Mediawiki container"
docker-compose stop

docker run --rm \
  -v semantic-mediawiki-docker_wiki-db:/data \
  -v ~/semantic-mediawiki-backup:/backup ubuntu bash \
  -c "cd /data && tar cvf /backup/semantic-mediawiki-docker_wiki-db.tar ."
docker run --rm \
  -v semantic-mediawiki-docker_wiki-images:/data \
  -v ~/semantic-mediawiki-backup:/backup ubuntu bash \
  -c "cd /data && tar cvf /backup/semantic-mediawiki-docker_wiki-images.tar ."
echo "Starting semantic mediawiki containers"
docker-compose start
cd ~

echo "Creating meta tar ball"
tar -cvzf ~/semantic-mediawiki-backup-$(date +"%m_%\d_%Y").tar.gz ./semantic-mediawiki-backup
sudo rm -r ./semantic-mediawiki-backup
echo "Backup complete"
'''

home_folder = find_home_folder()
subprocess.run(["mkdir", home_folder + "/semantic-mediawiki-backup"])
subprocess.run(["cp", "LocalSettings.php", home_folder + "/semantic-mediawiki-backup/"])
subprocess.run(["docker-compose", "stop"])

mah_volumes = get_volume_names()
#db_backup = docker_backup(mah_volumes["db_volume_name"], home_folder + "/semantic-mediawiki-backup",  "semantic-mediawiki-docker_wiki-db.tar")
#print(db_backup)
#subprocess.run(db_backup)
#image_folder_backup = docker_backup(mah_volumes["image_volume_name"], home_folder + "/semantic-mediawiki-backup",  "semantic-mediawiki-docker_wiki-images.tar")
#subprocess.run(image_folder_backup)
#subprocess.run(["cd", home_folder, "&&", "tar", "-cvzf", home_folder + '/semantic-mediawiki-backup-$(date +"%m_%d_%Y").tar.gz', "./semantic-mediawiki-backup"])

#bash_fun = 'docker run --rm -v semantic-mediawiki-docker_wiki-images:/data -v /home/dentropy/semantic-mediawiki-backup:/backup ubuntu bash -c "cd /data && tar cvf /backup/semantic-mediawiki-docker_wiki-images.tar ."'
bash_fun = 'docker run --rm -v %s:/data -v %s:/backup ubuntu bash -c "cd /data && tar cvf /backup/%s ."' % (mah_volumes["image_volume_name"], home_folder + "/semantic-mediawiki-backup", "semantic-mediawiki-docker_wiki-images.tar")
print(bash_fun)
process = os.system(bash_fun)
print("process:", process)

bash_fun = 'docker run --rm -v %s:/data -v %s:/backup ubuntu bash -c "cd /data && tar cvf /backup/%s ."' % (mah_volumes["db_volume_name"], home_folder + "/semantic-mediawiki-backup", "semantic-mediawiki-docker_wiki-db.tar")
print(bash_fun)
process = os.system(bash_fun)
print("process:", process)

print("Waiting 5 seconds")
time.sleep(5)

print('creating archive')
now = datetime.now()
cnow = datetime.now()
dt_string = now.strftime("%d-%m-%Y-%H-%M-%S")
backup_tar_name =  "/semantic-mediawiki-backup-" + dt_string + ".tar"
print("Backup tar file name is", backup_tar_name)
out = tarfile.open(home_folder + backup_tar_name, mode='w')
try:
    out.add(home_folder + "/semantic-mediawiki-backup/" + "semantic-mediawiki-docker_wiki-images.tar")
    out.add(home_folder + "/semantic-mediawiki-backup/" + "semantic-mediawiki-docker_wiki-db.tar")
    out.add(home_folder + "/semantic-mediawiki-backup/" + "LocalSettings.php")
finally:
    print('closing tar archive')
    out.close()

print('Contents of archived file:')
t = tarfile.open(home_folder + backup_tar_name, 'r')
for member in t.getmembers():
    print(member.name)

subprocess.run(["rm", "-r", home_folder + "/semantic-mediawiki-backup"])

os.system("docker-compose exec smw php maintenance/update.php")
