#!/usr/bin/python3

import subprocess
import fileinput
import os
import tarfile

def string_matcher(match_these, with_this):
    # First argument is a list of the small string you are looking for in a big string
    # Second argument is a list of big strings
    return [s for s in with_this if any(xs in s for xs in match_these)]

def docker_restore(volume_name, backup_dir, tar_file):
    restore_list = ["docker", "run", "--rm"]
    restore_list.append("-v")
    restore_list.append(volume_name + ":/recover")
    restore_list.append("-v")
    restore_list.append(backup_dir + ":/backup")
    restore_list.append("ubuntu")
    restore_list.append("bash")
    restore_list.append("-c")
    restore_list.append('"cd /recover && tar xvf /backup/' + tar_file +'"')
    return restore_list

def get_volume_names():
    raw_docker_volumes = str(subprocess.check_output(["docker", "volume", "ls"])).split()
    docker_volumes = []
    for i in raw_docker_volumes:
        tmp_docker_volumes = i.split("\\n")
        for i in tmp_docker_volumes:
            docker_volumes.append(i)
    volume_names = string_matcher(["semantic-mediawiki", "db", "images"], docker_volumes)
    mah_volumes = {}
    if "db" in volume_names[0]:
        mah_volumes["db_volume_name"] = volume_names[0]
        mah_volumes["image_volume_name"] = volume_names[1]
    else:
        mah_volumes["db_volume_name"] = volume_names[1]
        mah_volumes["image_volume_name"] = volume_names[0]
    return mah_volumes

def find_home_folder():
    pwd = str(subprocess.check_output(["pwd"]))
    home_list = pwd.split("/")
    home_folder = "/" + home_list[1] + "/" + home_list[2]
    return home_folder

for_starters = "      #- ./LocalSettings.php:/var/www/html/LocalSettings.php:ro"
for_running  = "      - ./LocalSettings.php:/var/www/html/LocalSettings.php:ro"

with fileinput.FileInput("docker-compose.yaml", inplace=True, backup='.bak') as file:
    for line in file:
        print(line.replace(for_running, for_starters), end='')

subprocess.run(["docker-compose", "up", "-d"])
subprocess.run(["docker-compose", "down"])
home_folder = find_home_folder()
home_files = str(subprocess.check_output(["ls", home_folder]))[2:]
home_files = home_files.split("\\n")
backed_up_tar_ball = string_matcher(['semantic-mediawiki-backup','.tar'], home_files)
print(backed_up_tar_ball)
subprocess.run(["tar", "-xvf", home_folder + "/" + backed_up_tar_ball[0]])
mah_volumes = get_volume_names()

# Get name of tar files
t = tarfile.open(home_folder + "/" + backed_up_tar_ball[0], 'r')
tar_files = []
for member in t.getmembers():
    tar_files.append(member.name)
print(tar_files)

#db_volume = subprocess.run(["docker", "volume", "create", mah_volumes["db_volume_name"]])
#image_volume = subprocess.run(["docker", "volume", "create", mah_volumes["image_volume_name"]])
#docker_restore(image_volume_name, pwd + "/semantic-mediawiki-backup", "semantic-mediawiki-docker_wiki-images.tar")
#docker_restore(db_volume_name, pwd + "/semantic-mediawiki-backup", "semantic-mediawiki-docker_wiki-db.tar")


# Copy over LocalSettings.php
pwd = str(subprocess.check_output(["pwd"]))[2:-3]
for i in tar_files:
    if "LocalSettings.php" in i:
        mah_file = "/" + str(i)
copy_LocalSettings = subprocess.check_output(["cp", pwd + mah_file, "LocalSettings.php"])


# Copy Volumes over
for i in tar_files:
    if "db" in i:
        tmp_mah_file = pwd  + "/" +  str(i)
mah_split = tmp_mah_file.split("/")
mah_file = ""
for i in range(len(mah_split)):
    if i == 0:
        mah_file += ""
    elif i == len(mah_split) - 1:
        print("Ooooooooo")
    else:
        mah_file += "/" + mah_split[i]


bash_fun = 'docker run --rm -v %s:/recover -v %s:/backup ubuntu bash -c "cd /recover && tar xvf /backup/%s"' % (mah_volumes["db_volume_name"],  mah_file, "semantic-mediawiki-docker_wiki-db.tar")
print(bash_fun)
process = os.system(bash_fun)
print("process:", process)

for i in tar_files:
    if "image" in i:
        tmp_mah_file = pwd + "/" + str(i)
mah_split = tmp_mah_file.split("/")
mah_file = ""
for i in range(len(mah_split)):
    if i == 0:
        mah_file += ""
    elif i == len(mah_split) - 1:
        print("Ooooooooo")
    else:
        mah_file += "/" + mah_split[i]
print(mah_file)

bash_fun = 'docker run --rm -v %s:/recover -v %s:/backup ubuntu bash -c "cd /recover && tar xvf /backup/%s"' % (mah_volumes["image_volume_name"], mah_file, "semantic-mediawiki-docker_wiki-images.tar")
print(bash_fun)
process = os.system(bash_fun)
print("process:", process)

with fileinput.FileInput("docker-compose.yaml", inplace=True, backup='.bak') as file:
    for line in file:
        print(line.replace(for_starters, for_running), end='')
subprocess.run(["docker-compose", "up", "-d"])

os.system("docker-compose exec smw php maintenance/update.php")

dat_bash = '''
cd ~
tar -xvf ~/semantic-mediawiki-backup*
cd ~/semantic-mediawiki-backup
tar -xvf srv.tar -C /srv
docker volume create semantic-mediawiki-docker_wiki-db
docker volume create semantic-mediawiki-docker_wiki-images
docker run --rm \
  -v semantic-mediawiki-docker_wiki-db:/recover \
  -v /home/dentropy/Projects/semantic-mediawiki-docker/semantic-mediawiki-backup:/backup ubuntu bash \
  -c "cd /recover && tar xvf /backup/semantic-mediawiki-docker_wiki-db.tar"
docker run --rm \
  -v semantic-mediawiki-docker_wiki-images:/recover \
  -v/home/dentropy/Projects/semantic-mediawiki-docker/semantic-mediawiki-backup:/backup ubuntu bash \
  -c "cd /recover && tar xvf /backup/semantic-mediawiki-docker_wiki-images.tar"
cp ~/semantic-mediawiki-backup/LocalSettings.php LocalSettings.php
'''