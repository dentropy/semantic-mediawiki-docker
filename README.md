# Semantic MediaWiki

Create and start a Docker image that contains a _vanilla_ [Semantic MediaWiki][1]

## Easy Instructions

**Deploy with backups**

``` bash
# Make sure this codebase is within same home folder as backup and restore scripts will not work
docker-compose up -d
# Go set up the wiki in the browser and download LocalSettings.php
python3 copy_LocalSettings.py
sudo python3 backup.py
```

**Restore**

``` bash
# Put tar backup in home directory
python3 restore.py
```

## Step 1: Get MediaWiki up and running

The intent is to run through the initial MediaWiki setup, have MediaWiki generate a 
`LocalSettgins.php`, and end up with a working plain MediaWiki:

* Trigger inital build and start the image: 

  ```
  :> docker-compose up -d
  ```

* Setup MediaWiki via its web based GUI at http://localhost:8081

  _Note:_ make sure to select **SQLite** as the database type!

* Download and save the `LocalSettings.php` into this folder

* Stop the running setup container and remove it

  ```
  :> docker-compose down
  ```

* Open the `docker-compose.yaml`, uncomment the line mounting the `LocalSettings.php`, save the file

* Test the wiki (now with the setup stored in `LocalSettings.php`)

  ```
  :> docker-compose up -d
  ```


## Step 2: Activate Semantic MediaWiki

This section follows the install guideline of [Semantic MediaWiki][1] at their [guide][2].

* Stop the running container

  ```
  :> docker-compose stop
  ```

* Append `enableSemantics()` to the end of `LocalSettings.php`

  ```
  :> echo "enableSemantics();" >> LocalSettings.php
  ```
  _Note:_ as this is a proof-of-concept, the host parameter to `enableSemantics()` is empty. (According
  to [3] this shouldn't be a problem.) 
  
* Start the configured Semantic MediaWiki

  ```
  :> docker-compose start
  ```

* Run `update.php`
 
  ```
  :> docker-compose exec smw php maintenance/update.php
  ```  

* Test your vanilla SMW  at http://localhost:8081
    

[1]: https://www.semantic-mediawiki.org
[2]: https://www.semantic-mediawiki.org/wiki/Help:Installation/Quick_guide
[3]: https://www.semantic-mediawiki.org/w/index.php?title=Help:EnableSemantics
